package com.example.test.control;

import com.example.test.repository.UserRepository;
import com.example.test.service.UserService;
import com.example.test.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extensions;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@Extensions({@ExtendWith(MockitoExtension.class)})
 class UserServiceTest {
    @Mock
    private UserRepository userRepository;


    private UserService userService;

    @BeforeEach
    void setup(){
     MockitoAnnotations.openMocks(this);
     this.userService=new UserServiceImpl(this.userRepository);
 }

    @Test
    void testFindUsers() {
        userRepository.findAll();
        Mockito.verify(userRepository, Mockito.times(1)).findAll();
    }
    @Test
    void test(){
        userService.addUser("dwoi","devorak","gg@gmail.com");
        Mockito.verify(userRepository, Mockito.times(1)).findAll();
    }
}
