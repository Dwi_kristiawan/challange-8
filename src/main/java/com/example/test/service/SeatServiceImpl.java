package com.example.test.service;

import com.example.test.model.Seat;
import com.example.test.repository.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class SeatServiceImpl implements SeatService{
    @PersistenceContext
    private EntityManager em;
    @Autowired
    SeatRepository seatRepository;
    @Autowired
    UserService userService;
    @Autowired
    ScheduleService scheduleService;


    @Override
    public void addSeat(String studioName, String nomorKursi, Integer userId, Integer scheduleId) {
        Seat seat=new Seat();
        seat.setStudioName(studioName);
        seat.setNomorKursi(nomorKursi);
        seat.setUserId(userService.findUserById(userId));
        seat.setSchedulesId(scheduleService.findById(scheduleId));
        seatRepository.save(seat);


    }

    @Override
    public List<Seat> invoice(String username) {
        return seatRepository.ShowInvoice(username);
    }

    @Override
    public List<Seat> showSeat() {
        return seatRepository.findAll();
    }

    @Override
    public void deleteSeat(String studioName, String nomorKursi) {
        seatRepository.deleteByStudioNameAndNomorKursi(studioName,nomorKursi);
    }

}
