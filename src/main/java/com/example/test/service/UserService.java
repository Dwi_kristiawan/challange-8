package com.example.test.service;

import com.example.test.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    void addUser(String username,String password,String email);

    User findUserById(Integer userId);//keperluan Foreign key
    List<User> getUserById(Integer userId);
    void deleteUserById(Integer userId);
    void updateUser(Integer userId,String username,String password, String email);
    List<User>userList();


}
