package com.example.test.service;

import com.example.test.model.Film;
import com.example.test.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class FilmServiceImpl  implements FilmService {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    FilmRepository filmRepository;
    @Override
    public void addFilm(String filmName, String statusTayang) {
        Film film=new Film();
        film.setFilmName(filmName);
        film.setStatusTayang(statusTayang);
        filmRepository.save(film);
    }

    @Override
    public List<Film> showFilm() {
        List<Film>film=filmRepository.findAll();
        return film;
    }

    //keperluan foreign
    @Override
    public Film getFilmById(Integer filmCode) {
        Film film=new Film();
        return filmRepository.getById(filmCode);
    }

    @Override
    public void updateFilm(Integer filmCode, String filmName, String statusTayang) {
        filmRepository.updateFilm(filmCode,filmName,statusTayang);
    }

    @Override
    public void deleteFilm( Integer filmCode) {
        filmRepository.deleteFilmsByFilmCode(filmCode);
    }

    @Override
    public List<Film> getByStatusTayang(String statusTayang) {
        return filmRepository.findByStatusTayang(statusTayang);
    }

    @Override
    public List<Film> getByFilmName(String filmName) {

        return filmRepository.findByFilmName(filmName);
    }
}
