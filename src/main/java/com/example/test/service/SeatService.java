package com.example.test.service;

import com.example.test.model.Seat;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SeatService {
    void addSeat(String studioName,String nomorKursi, Integer userId,Integer scheduleId);
    List<Seat> invoice(String username);
    List<Seat> showSeat();
    void deleteSeat(String studioName, String nomorKursi);
}
