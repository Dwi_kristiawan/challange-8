package com.example.test.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id")
    private Integer scheduleId;

    @Column(name = "tanggal_tayang")
    private String tanggalTayang;

    @Column(name = "harga_tiket")
    private Integer hargaTiket;


    @Column(name = "jam_mulai")
    private String jamMulai;

    @Column(name = "jam_selesai")
    private String jamSelesai;

    @ManyToOne
    @JoinColumn(name = "film_code")
    private Film filmCode;
}
