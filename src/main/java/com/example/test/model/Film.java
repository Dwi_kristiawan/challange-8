package com.example.test.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity(name = "film")
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "film_code")
    private Integer filmCode;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "status_tayang")
    private String statusTayang;


}
