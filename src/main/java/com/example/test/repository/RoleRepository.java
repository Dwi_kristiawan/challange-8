package com.example.test.repository;

import com.example.test.enumeration.ERole;
import com.example.test.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Integer> {

    Optional<Roles> findByName (ERole name);
    Optional<Roles>findByRoleId(int roleId);
}
