package com.example.test.control;

import com.example.test.model.Film;
import com.example.test.service.FilmService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Tag(name = "film", description = "control pada film")
@RestController
@RequestMapping("/film")
public class FilmControl {
    @Autowired
    FilmService filmService;

    //Film
    @PostMapping("/admin/add-film")
    String addFilm(@RequestBody Film film){
        filmService.addFilm(film.getFilmName(),film.getStatusTayang());

        return "add film berhasil";
    }
    @GetMapping("/public/show-film")
    List<Film> showFilm(){
    List<Film>film=filmService.showFilm();
    return film;
    }
    @PutMapping("/admin/update-film")
    void updateFilm(@RequestBody Film film){
        filmService.updateFilm(film.getFilmCode(),film.getFilmName(),film.getStatusTayang());
    }
    @DeleteMapping("/admin/delete-film/{id}")
    void deleteFilmByFilmName(@PathVariable("id") Integer filmCode){
        filmService.deleteFilm(filmCode);
    }
    @GetMapping("/admin/show-film-by-status/{status}")
    public List<Film> showFilmByStatus( @PathVariable("status") String statusTayang){
        List<Film>film=filmService.getByStatusTayang(statusTayang);
        film.forEach(Film->{
                    System.out.println(Film.getFilmCode()+"\t\t\t"+Film.getFilmName()+"\t\t\t"+Film.getStatusTayang());
                }
        );
        return film;

    }
    @GetMapping("/public/show-film-by-name/{filmName}")
    List<Film>showFilmByFilmName(@PathVariable("filmName")String filmName){
        List<Film>film=filmService.getByFilmName(filmName);
        return film;
    }
    
}
